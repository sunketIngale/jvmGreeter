package greeter

import spock.lang.Specification

class GreetingFormatterSpec extends Specification {

    def 'Creating a greeting'() {

        expect: 'The greeeting to be correctly capitalized'
        GreetingFormatter.greeting('devops') == '''
  _   _          _   _                 ____                                       
 | | | |   ___  | | | |   ___         |  _ \\    ___  __   __   ___    _ __    ___ 
 | |_| |  / _ \\ | | | |  / _ \\        | | | |  / _ \\ \\ \\ / /  / _ \\  | '_ \\  / __|
 |  _  | |  __/ | | | | | (_) |  _    | |_| | |  __/  \\ V /  | (_) | | |_) | \\__ \\
 |_| |_|  \\___| |_| |_|  \\___/  ( )   |____/   \\___|   \\_/    \\___/  | .__/  |___/
                                |/                                   |_|          
'''[1..-1]
    }
}
