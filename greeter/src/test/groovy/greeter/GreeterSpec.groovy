package greeter

import spock.lang.Specification

class GreeterSpec extends Specification {

    def 'Calling the entry point'() {

        setup: 'Re-route standard out'
        def buf = new ByteArrayOutputStream(1024)
        System.out = new PrintStream(buf)

        when: 'The entrypoint is executed'
        Greeter.main('devops')

        then: 'The correct greeting is output'
        buf.toString() == '''
  _   _          _   _                 ____                                       
 | | | |   ___  | | | |   ___         |  _ \\    ___  __   __   ___    _ __    ___ 
 | |_| |  / _ \\ | | | |  / _ \\        | | | |  / _ \\ \\ \\ / /  / _ \\  | '_ \\  / __|
 |  _  | |  __/ | | | | | (_) |  _    | |_| | |  __/  \\ V /  | (_) | | |_) | \\__ \\
 |_| |_|  \\___| |_| |_|  \\___/  ( )   |____/   \\___|   \\_/    \\___/  | .__/  |___/
                                |/                                   |_|          

'''[1..-1]
    }
}
